# Install pandoc

VERSION=$(curl -s "https://api.github.com/repos/jgm/pandoc/releases/latest" | grep -Po '"tag_name": "\K.*?(?=")')

curl -sLo "pandoc-${VERSION}-1-amd64.deb" "https://github.com/jgm/pandoc/releases/download/${VERSION}/pandoc-${VERSION}-1-amd64.deb"

apt-get -y install "./pandoc-${VERSION}-1-amd64.deb"